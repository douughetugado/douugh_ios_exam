# Douugh_iOS_Exam

Coding test for aspiring Douugh iOS devs

![Screen Structure](examimage.png) 

Fetch all users from https://gist.githubusercontent.com/douughios/f3c382f543a303984c72abfc1d930af8/raw/5e6745333061fa010c64753dc7a80b3354ae324e/test-users.json

# Screen 1:
1.By default, should fetch and display all users alphabetically by first name (Barry White, ...) 

2.Should have a segmented control with 3 items: First, Last, ID

3.Selecting a segmented control should automatically sort the table's contents alphabetically using the selected segment

		a. First - by first name (Barry White, ...)
  		b. Last - by last name (Benjamin Joseph Mark John Peter Simon Luke Chiong, ...)
  		c. ID - by ID (Bob Ong, ...)
	
4.Cells should expand to accomodate the user's full name, if necessary

5.Tapping a cell should show Screen 2 and pass the user's details

# Screen2:
1.Should show the selected user's details

2.Pressing back should show Screen 1


# Design the UI programatically using constraints or SnapKit
# Use good design patterns